#!/bin/bash

diff=( $(git diff --dirstat=files,0 $CI_COMMIT_BEFORE_SHA | awk '{print $2}') )

for i in "${diff[@]}"
do
  echo "$i"
done;
